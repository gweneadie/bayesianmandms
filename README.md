# BayesianMandMs

This repository contains Jupyter notebooks that are intended to be used during the m&m's exercise described in the manuscript submitted to the Journal of Statistics Education. The notebooks may be useful for both instructors and students. 

One of the notebooks is for the standard exercise (written in R), and the other is for the hierarchical exercise (written in Python).

